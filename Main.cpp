#include <iostream>

void PrintNumbers(bool characterNumber, int n)
{
    for (int i = characterNumber; i < n; i += 2)
    {
        std::cout << i << " ";
    }
}

int main()
{
    const int n = 10;

    //������� 1. ������� � ������� ��� ������ ����� �� 0 �� N
    for (int i = 0; i <= 10; i++) 
    {
        if (i % 2 == 0) 
        {
            std::cout << i << " ";
        }
    }
    
    std::cout << '\n';
    
    //������� 2. �������� �������, ������� � ����������� �� ����� ����������
    //�������� � ������� ���� ������, ���� �������� ����� �� 0 �� N
    PrintNumbers(true, n);
}